---
title: Serverless with AWS
date: 2019-06-11
tags: ["serverless", "AWS"]
---

For my day job, I've been working on a project that utilizes Amazon's services for serverless applications.
Here I hope to share some of my helpful takeaways.

## Serverless vs. Alternatives

[Serverless](https://serverless.com/) is a Node package that enables configuration of serverless cloud services by means of a single YAML file.
When researching how to automate creation of AWS resources, I briefly tried using [Ansible](https://www.ansible.com/), but found that I would have to detail each resource I needed.
Similarly, writing a CloudFormation template manually would require the same detailed knowledge of each required AWS resource.
The developers on the project already had experience with Node JS development, so having another Node package that would automatically package code for AWS Lambda functions and create a CloudFormation stack was rather convenient.

## Serverless isn't Everything

However helpful Serverless was in packaging and deploying the application, it still couldn't do everything we needed it to do.
We needed some IAM accounts created, some roles for deploying the application, and some secrets stored in AWS Secrets Manager.
For the extra setup, we used [Terraform](https://www.terraform.io/).
Everything fit nicely into a few files, and was easy to deploy.
If our AWS setup already included those accounts and permissions, we may have been able to get by with just Serverless, but Terraform was helpful for the extra steps.

## Documentation

Serverless has pretty good documentation on its site, but it isn't comprehensive.
I wasn't able to find sufficient examples or instructions for setting up a CloudFront distribution.
However, I discovered that under the hood, Serverless was using the YAML in its own configuration file to create a CloudFormation template with the same YAML.
So, a lot of the documentation that was helpful for CloudFront came from Amazon's own documentation.
