---
title: CV
---

---

## Core Competencies

* Infrastructure management with Terraform
* Automation and tooling development with Bash, Python, and Go
* Linux server management with CLI
* Software development and deployment pipelines with GitLab

## Recent Experience

### Federal Reserve Bank of Cleveland

#### Senior DevOps Engineer: Apr 2022 - Present

Responsibilities:

* Architect and implement self-hosted deployments of commercial applications in AWS GovCloud
* Maintain and support applications for software development tools, including Atlassian products, JFrog Platform, and GitLab Enterprise
* Mentor junior members of the team

Achievements:

* Designed and implemented greenfield AWS cloud infrastructure automation with Terraform and GitLab pipelines
* Designed and implemented cloud-native automation tooling with Terraform and Go
* Transitioned software teams from Sonatype Nexus & NexusIQ to JFrog Artifactory & Xray
* Assumed responsibility for existing GitLab Enterprise deployment and reimplement infrastructure pipeline

Skills developed:

* AWS cloud infrastructure automation with GitLab and Terraform
* Teaching DevOps skills by mentoring junior engineers

#### DevOps Engineer: Oct 2019 - Apr 2022

Responsibilities:

* Maintain and support applications for software development teams, like Atlassian products and Nexus artifact storage
* Transition teams from on-premise applications to applications deployed in AWS GovCloud

Achievements:

* Deployed Mattermost in AWS GovCloud to teams across multiple bank districts
* Implemented infrastructure build pipeline using Ansible and Terraform

Skills developed:

* AWS cloud infrastructure automation with Ansible, Packer, and Terraform
* Customer service and relationships with developers

### American Greetings Interactive, Inc

#### Associate Web Operations Engineer: Dec 2016 - Sept 2019

Responsibilities:

* Maintained Linux Virtual Machine infrastructure in co-location data center and AWS
* Managed servers with custom configuration management software and with Ansible
* Performed code change control operations for Python & Oracle Commerce platform websites
* Developed tools for automated software deployments
* Supported daily developer activities, including troubleshooting applications

Achievements:

* Revised and standardized build and deployment processes utilizing Ansible and Jenkins
* Transitioned organization from Atlassian HipChat to Slack
* Refined static asset deployment pipeline, automating manual processes
* Developed an automated deployment pipeline for AWS serverless applications

Skills developed:

* Linux server management
* Enterprise-scale development operations practices
* Ability to quickly learn new code bases and infrastructure and adapt to changes
* Communication and collaboration among teams with varying skill sets and backgrounds

## Education

### John Carroll University: 2012-2016

Bachelor of Science (B.S.) in Mathematics and Computer Science

## Certifications

* AWS Certified Cloud Practitioner (2023-2026)

