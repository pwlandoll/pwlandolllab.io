---
title: About Me
---

I'm a DevOps engineer from Cleveland, OH, and a graduate of John Carroll University with a B.S. in Mathematics & Computer Science.

My primary interests include:

* Linux and open-source software
* Knitting (see me on [Ravelry](https://www.ravelry.com/people/pwlandoll))
* Tabletop RPGs

Lately I've been getting into (and would love to talk about):

* Go
* Spinning & other fiber arts

## How I ended up here

Born, raised, and educated in Cleveland.
Like many, my interest in technology started with video games.
I discovered Linux during high school in an effort to avoid Windows errors in certain PC games, which despite not solving my problems, led me down the road of Open Source Software.
From there it didn't take long for that interest to turn into a desire to program professionally.
My goal is to be able to use those skills to contribute to Open Source communities.
